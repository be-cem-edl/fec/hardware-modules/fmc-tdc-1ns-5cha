"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2020 CERN
"""
from pathlib import Path
import pytest
import subprocess
import time
import re
import os
from PyFmcTdc import FmcTdc


def valid_slot_type(slot):
    if re.search(r"(VME|PCI)\.[0-9]+-FMC\.[0-9]+", slot) is None:
        raise ValueError()
    return slot


def id_from_slot(slot, name):
    carrier, mezzanine = slot.split("-")
    carrier_bus, carrier_slot = carrier.split(".")
    carrier_slot = int(carrier_slot)
    mezzanine_bus, mezzanine_slot = mezzanine.split(".")
    mezzanine_slot = int(mezzanine_slot)
    if carrier_bus == "PCI":
        with open("/run/dynpci") as f:
            for line in f.readlines():
                dynslot, pciid = line.strip().split(" ")
                if int(dynslot) == carrier_slot:
                    break
        pciid = f"0000:{pciid}"
        pathfmc = list(Path("/sys/bus/pci/devices").joinpath(pciid)
                                                   .glob(f"spec-*/id:*/{name}.*.auto/fmc-slot-*.{mezzanine_slot}"))
    elif carrier_bus == "VME":

        pathfmc = list(Path("/sys/bus/vme/devices").joinpath(f"slot.{carrier_slot:02d}")
                                                   .joinpath(f"vme.{carrier_slot:02d}")
                                                   .glob(f"svec-*/svec-*/id:*/{name}.*.auto/fmc-slot-*.{mezzanine_slot}"))
    else:
        raise ValueError()
    assert len(pathfmc) == 1
    devname = list(Path(pathfmc[0]).parent.glob("hw-*/*-????/devname"))
    assert len(devname) == 1
    with open(devname[0]) as f:
        fd_id = int(f.read().strip().split("-")[-1], 16)
    return fd_id


def pytest_generate_tests(metafunc):
    if "fd_tdc_id" in metafunc.fixturenames:
        metafunc.parametrize("fd_tdc_id", pytest.fd_tdc_id)


class PulseGenerator(object):
    def __init__(self, id):
        self.id = id

    def disable(self, ch):
        pass
    def generate_pulse(self, ch, rel_time_us,
                       period_ns, count, sync):
        pass


class SCPI(PulseGenerator):
    def __init__(self, scpi_id):
        super(SCPI, self).__init__(scpi_id)
        import pyvisa
        self.mgr = pyvisa.ResourceManager()
        self.instr = self.mgr.open_resource(self.id)
        self.instr.query_delay = 0
        self.instr.timeout = 10000
        self.instr.read_termination = '\n'
        self.instr.write_termination = '\n'
        self.instr.write("*RST")
        self.instr.query_ascii_values("*OPC?")
        self.instr.write("*CLS")
        self.instr.write("INITIATE:CONTINUOUS OFF")
        self.instr.write("OUTPUT:STATE OFF")

    def disable(self, ch):
        self.instr.write("OUTPUT:STATE OFF")

    def generate_pulse(self, ch, rel_time_us,
                       period_ns, count, sync):
        self.instr.write("OUTPUT:STATE OFF")
        # START Custom Agilent 33600A commands
        self.instr.write("SOURCE:BURST:STATE OFF")
        # END Custom Agilent 33600A commands

        self.instr.write("SOURCE:VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE 2.5V")
        self.instr.write("SOURCE:VOLTAGE:LEVEL:IMMEDIATE:OFFSET 1.25V")
        self.instr.write("SOURCE:FUNCTION:SHAPE PULSE")
        self.instr.write("SOURCE:PULSE:WIDTH 101ns")
        self.instr.write("SOURCE:PULSE:PERIOD {:d}ns".format(period_ns))

        # START Custom Agilent 33600A commands
        self.instr.write("TRIGGER:DELAY {:d}e-6".format(rel_time_us))

        burst_period_ns = int(count / (1 / period_ns)) + 500
        self.instr.write("SOURCE:BURST:INTERNAL:PERIOD {:d}ns".format(burst_period_ns))
        self.instr.write("SOURCE:BURST:NCYCLES {:d}".format(count))
        self.instr.write("SOURCE:BURST:STATE ON")
        # END Custom Agilent 33600A commands
        self.instr.write("OUTPUT:STATE ON")

        self.instr.query_ascii_values("*OPC?")
        self.instr.write("INITIATE:IMMEDIATE")
        if sync:
            self.instr.query_ascii_values("*OPC?")


class FmcFineDelay(PulseGenerator):
    CHANNEL_NUMBER = 4

    def __init__(self, fd_id):
        super(FmcFineDelay, self).__init__(fd_id)

    def disable(self, ch):
        cmd = ["fmc-fdelay-pulse",
               "-d", "0x{:x}".format(self.id),
               "-o", str(ch),
               "-m", "disable",
               ]
        proc = subprocess.Popen(cmd)
        proc.wait()

    def generate_pulse(self, ch, rel_time_us,
                       period_ns, count, sync):
        cmd = ["fmc-fdelay-pulse",
               "-d", "0x{:x}".format(self.id),
               "-o", str(ch),
               "-m", "pulse",
               "-r", "{:d}u".format(rel_time_us),
               "-T", "{:d}n".format(period_ns),
               "-w", "{:d}n".format(int(period_ns / 2)),
               "-c", str(count),
               "-t"
               ]
        proc = subprocess.Popen(cmd)
        proc.wait()
        if sync:
            time.sleep(1 + 2 * (period_ns * count) / 1000000000.0)


@pytest.fixture(scope="function")
def tdc_and_gen(fd_tdc_id):
    fd_id, tdc_id = fd_tdc_id
    gen = FmcFineDelay(fd_id)
    for ch in range(FmcFineDelay.CHANNEL_NUMBER):
        gen.disable(ch + 1)

    tdc = FmcTdc(tdc_id)
    for ch in tdc.chan:
        ch.enable = False
        ch.termination = False
        ch.timestamp_mode = "post"
        ch.flush()
    yield (gen, tdc)
    for ch in range(FmcFineDelay.CHANNEL_NUMBER):
        gen.disable(ch + 1)
    for ch in tdc.chan:
        ch.enable = False
        ch.termination = False
        ch.timestamp_mode = "post"
        ch.flush()


@pytest.fixture(scope="function")
def fmcfd(tdc_and_gen):
    fd, tdc = tdc_and_gen
    yield fd


@pytest.fixture(scope="function")
def fmctdc(tdc_and_gen):
    fd, tdc = tdc_and_gen
    yield tdc


def pytest_addoption(parser):
    parser.addoption("--tdc-id", type=lambda x: int(x, 16), action="append",
                     default=[], help="Fmc TDC Linux Identifier")
    parser.addoption("--slot-tdc", type=valid_slot_type, action='append',
                     default=[], help="Fmc TDC absolute slot (works only for SPEC and SVEC)")
    parser.addoption("--fd-id", type=lambda x: int(x, 16), action="append",
                     default=[], help="Fmc Fine-Delay Linux Identifier")
    parser.addoption("--slot-fd", type=valid_slot_type, action='append',
                     default=[], help="Fmc Fine-Delay absolute slot (works only for SPEC and SVEC)")
    parser.addoption("--dump-range", type=int, default=10,
                     help="Timestamps to show before and after an error")
    parser.addoption("--channel", type=int, default=[],
                     action="append", choices=range(FmcTdc.CHANNEL_NUMBER),
                     help="Channel(s) to be used for acquisition tests. Default all channels")
    parser.addoption("--usr-acq-count", type=int, default=0,
                     help="Number of pulses to generate during a acquisition test.")
    parser.addoption("--usr-acq-period-ns", type=int, default=0,
                     help="Pulses period (ns) during a acquisition test.")


def id_list_get(config, opt_id, opt_slot, devname):
    id_list = config.getoption(opt_id)
    if len(id_list) == 0:
        slot = config.getoption(opt_slot)
        if len(slot) == 0:
            print(f"Missing argument {opt_id} or {opt_slot}")
            raise Exception()
        id_list = []
        for slot in slot:
            id_list.append(id_from_slot(slot, devname))
    return id_list


def pytest_configure(config):
    pytest.tdc_id = id_list_get(config, "--tdc-id", "--slot-tdc", "fmc-tdc")
    pytest.fd_id = id_list_get(config, "--fd-id", "--slot-fd", "fmc-fdelay-tdc")

    if len(pytest.tdc_id) != len(pytest.fd_id):
        print("For each --fd-id there must be a --tdc-id")
        raise Exception()

    pytest.fd_tdc_id = list(zip(pytest.fd_id, pytest.tdc_id))

    pytest.channels = config.getoption("--channel")
    if len(pytest.channels) == 0:
        pytest.channels = range(FmcTdc.CHANNEL_NUMBER)

    pytest.usr_acq = (config.getoption("--usr-acq-period-ns"),
                      config.getoption("--usr-acq-count"))
    pytest.dump_range = config.getoption("--dump-range")

    pytest.transfer_mode = None
    modes = []
    for tdc_id in pytest.tdc_id:
        with open(f"/sys/bus/zio/devices/tdc-1n5c-{tdc_id:04x}/transfer-mode") as f_mode:
            modes.append(int(f_mode.read().rstrip()))
    mode = set(modes)
    if len(mode) != 1:
        print("Inconsistent transfer mode across devices")
        raise Exception()
    for k, v in FmcTdc.TRANSFER_MODE.items():
        if v in mode:
            pytest.transfer_mode = k

    pytest.carrier = None
    carrs = []
    for tdc_id in pytest.tdc_id:
        full_path = os.readlink("/sys/bus/zio/devices/tdc-1n5c-{:04x}".format(tdc_id))
        for carr in ["spec", "svec"]:
            is_carr = re.search(carr, full_path)
            if is_carr is None:
                continue
            if pytest.carrier is not None and pytest.carrier != carr:
                print("Inconsistent installation mix of SPEC and SVEC")
                raise Exception()
            pytest.carrier = carr
